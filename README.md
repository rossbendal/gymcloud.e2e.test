# README #

Repository for GymCloud's webapp end-to-end testing.

### Pre-requisite ###

* Java / JDK
* Node.js
* NPM

### Installing and Starting ###

```
npm install -g protractor 
webdriver-manager update
webdriver-manager start
```

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact