/**
 * Created by Ross Bendal on 05/05/2016.
 */

function allure_report_jetty_deploy() {
    console.log('Generating allure reports from xml using maven plugin and deploying them on port:1234[localhost or jenkins node ip] via jetty server.It should not take more than 1 minute......');
    console.log('If at times there is some issue in report deployment or reports are not available on mentioned port, please restart jenkins master and re run the test build');

    var exec = require('child_process').exec;

    function puts(error, stdout, stderr) {
        console.log(stdout)
    }
    exec("mvn site -Dallure.results_pattern=allure-results && mvn jetty:run -Djetty.port=1234", puts);
    var startTimes = Date.now();
    while (Date.now() - startTimes < 60000) {
    }
}

function send_mail() {
    console.log("Sending Mail with reports for the test execution.");
    var sys = require('util')
    var exec = require('child_process').exec;

    function puts(error, stdout, stderr) {
        console.log(stdout)
    }
    exec("node /Users/rab/Documents/GymCloud/gymcloud.e2e.test/util/mail.js", puts);
}

exports.config = {

	// The address of a running selenium server.
	//seleniumAddress: 'http://localhost:4444/wd/hub',

	// Capabilities to be passed to the webdriver instance.
	capabilities: {
		'browserName': 'chrome'
	},

	// Spec patters a re relative to the current directory when protractor is called.

	specs: ['../test_spec/draft/*_spec.js'],

    suites: {
        landing: ['../test_spec/landing/*_spec.js'],
        webapp: ['../test_spec/webapp/public/*_spec.js',
                '../test_spec/webapp/client/*_spec.js',
                '../test_spec/webapp/pro/*_spec.js']
    },

	// Options to be passed to Jasmine-node.
	jasmineNodeOpts: {
		showColors: true,
		defaultTimeoutInterval: 200000
	},

    onPrepare: function() {

        console.log('Deleting old allure reports and files.');
        var sys = require('util')
        var exec = require('child_process').exec;

        function puts(error, stdout, stderr) {
            console.log(stdout)
        }

        exec("rm -rf /Users/rab/Documents/GymCloud/gymcloud.e2e.test/allure-results", puts);
        exec("rm -rf /Users/rab/Documents/GymCloud/gymcloud.e2e.test/conf/allure-results", puts);
        exec("rm -rf /Users/rab/Documents/GymCloud/gymcloud.e2e.test/conf/target", puts);

        var AllureReporter = require('jasmine-allure-reporter');
        jasmine.getEnv().addReporter(new AllureReporter({
            allureReport: {
                resultsDir: '/Users/rab/Documents/GymCloud/gymcloud.e2e.test/allure-results'
            }
        }));
        //used to take screenshots
        jasmine.getEnv().afterEach(function(done){
            browser.takeScreenshot().then(function (png) {
                allure.createAttachment('Screenshot', function () {
                    return new Buffer(png, 'base64')
                }, 'image/png')();
                done();
            })
        });
        console.log('Stopping jetty server if any previous instance is running on port 1234.')

        exec("mvn jetty:stop -Djetty.port=1234", puts);
        var startTimer = Date.now();
        while (Date.now() - startTimer < 10000) {
        }
    },

    onComplete:function () {
        allure_report_jetty_deploy();
        //send_mail();
    }

};