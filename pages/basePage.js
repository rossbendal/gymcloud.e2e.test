/**
 * Created by Ross Bendal on 05/05/2016.
 */

var BasePage = function(){

	var landingPage = "https://gymcloud:GCstaging@www.s.gymcloud.com";
    var logInPage   = "https://gymcloud:GCstaging@app.s.gymcloud.com/#login";
    var basePage = "https://gymcloud:GCstaging@app.s.gymcloud.com";

	this.navigateToLandingPage = function(){
		browser.get(landingPage);
        return require('./landing/landingPage.js');
	};

    this.navigateToAboutUsPage = function(){
        browser.get(landingPage+"/about");
        return require('./landing/aboutUsPage.js');
    };

    this.navigateToPricingPage = function(){
        browser.get(landingPage+"/pricing");
        return require('./landing/pricingPage.js');
    };

    this.navigateToContactUsPage = function(){
        browser.get(landingPage+"/contact");
        return require('./landing/contactUsPage.js');
    };

    this.navigateToBlogPage = function(){
        browser.get(landingPage+"/blog");
        return require('./landing/blogPage.js');
    };

    this.navigateToLogInPage = function(){
        browser.get(logInPage);
        return require('./webapp/logInPage.js');
    };
    
    this.navigateToSignUpPage = function(){
        browser.get(basePage+"/#signup");
        return require('./webapp/signUpPage.js');
    };

	this.getPageTitle = function(){
		return browser.getTitle();
	};

};

module.exports = new BasePage();