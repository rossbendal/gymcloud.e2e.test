var EC = protractor.ExpectedConditions;

var AboutUsPage = function(){

	this.clickPricingHeaderButton = function(){
	element(by.css(".navbar-right [href='/pricing']")).click();
	var e = element(by.cssContainingText("h1","Pricing"));
	browser.wait(EC.presenceOf(e),10000);
	expect(e.isPresent()).toBeTruthy();
	return require('./pricingPage.js');
	};

};

module.exports = new AboutUsPage();