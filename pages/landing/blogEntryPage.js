/**
 * Created by rab on 02/06/2016.
 */
var webUtil = require('../../util/WebUtil.js');

var BlogEntryPage = function(){

    this.clickTwitterShare = function(){
        webUtil.waitCssUntilVisible(".single-details__social-buttons .addthis_button_tweet");
        element(by.css("#twitter-widget-0.twitter-share-button.twitter-share-button-rendered.twitter-tweet-button")).click();
        browser.getAllWindowHandles().then(function(handles){
            browser.switchTo().window(handles[1]).then(function(){
                webUtil.waitCssUntilVisible(".submit[type='submit']");
                browser.driver.close();
                browser.driver.switchTo().window(handles[0]);
            });
        });
        return this;
    };

    this.clickFBShare = function(){
        webUtil.waitCssUntilVisible("[allowtransparency='true'][title='fb:share_button Facebook Social Plugin']");
        element(by.css("[allowtransparency='true'][title='fb:share_button Facebook Social Plugin']")).click();
        browser.getAllWindowHandles().then(function(handles){
            browser.switchTo().window(handles[1]).then(function(){
                webUtil.waitCssUntilVisible("[name='login']");
                browser.driver.close();
                browser.driver.switchTo().window(handles[0]);
            });
        });
        return this;
    };

};

module.exports = new BlogEntryPage();