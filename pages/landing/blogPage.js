/**
 * Created by rab on 01/06/2016.
 */

var webUtil = require('../../util/WebUtil.js');

var BlogPage = function(){

    this.clickReadMoreLink = function(){
        webUtil.waitCssUntilVisible(".blog-post__full-link");
        element(by.css(".blog-post__full-link")).click();
        webUtil.waitCssUntilVisible(".single-post__title");
        return require('./blogEntryPage.js');
    };

};

module.exports = new BlogPage();