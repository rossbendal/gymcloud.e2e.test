var EC = protractor.ExpectedConditions;

var ContactUsPage = function(){

	this.clickBlogHeaderButton = function(){
    	element(by.css(".navbar-right [href='/blog']")).click();
    	var e = element(by.css(".subscribe-header__title"));
    	browser.wait(EC.presenceOf(e),10000);
    	expect(e.isPresent()).toBeTruthy();
    	return require('./blogPage.js');
	};

	this.fillUpContactForm = function(){
        element(by.css("[name='full_name']")).sendKeys("Ross - Automated Test");
        element(by.css("[type='email']")).sendKeys("ross@test.com");
        element(by.css("[name='message']")).sendKeys("Please disregard this test, this is ross and my automated test.");
        return this;
    };

    this.clickSubmit = function(){
        element(by.css("[type='submit']")).click();
        return this;
    };

};

module.exports = new ContactUsPage();