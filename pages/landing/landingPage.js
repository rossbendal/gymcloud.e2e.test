/**
 * Created by Ross Bendal on 07/05/2016.
 */

var webUtil = require('../../util/WebUtil.js');

var LandingPage = function() {

	this.fillUpDemoForm = function(){
		element(by.css(".button--header-btn")).click();
        webUtil.waitCssUntilVisible('.popup__title-text');
        element(by.css("[name='first_name']")).sendKeys("justTest");
        element(by.css("[name='last_name']")).sendKeys("Please disregard");
		element(by.css("[name='business_name']")).sendKeys("testBusinesss Name Only!, This is Ross");
		element(by.css("[name='email']")).sendKeys("test@ross.com");
		element(by.css("[name='phone']")).sendKeys("2344434");
		element(by.css("[type='submit']")).click();
        webUtil.waitCssUntilVisible(".alert.alert-success");
		return this;
	};

	this.clickSignUpNowButton = function(){
		element(by.css(".button--buy-btn")).click();
        webUtil.waitCssUntilVisible(".popup__title-text");

		return this;
	};

	this.clickTryfreefordaysButton = function(){
		element(by.css(".button--footer-btn")).click();
        webUtil.waitCssUntilVisible(".button--footer-btn");
		return this;
	};

	this.clickAboutUsHeaderButton = function(){
		element(by.css(".main-header__nav-link[href='/about']")).click();
        webUtil.waitTextUntilVisible(".col-md-offset-6 h1","Our Mission");
		return require('./aboutUsPage.js');
	};

	this.clickContactUsHeaderButton = function(){
		element(by.css(".main-header__nav-link[href='/contact']")).click();
		webUtil.waitTextUntilVisible("h1", "Contact");
		return require('./contactUsPage.js');
	};

	
};

module.exports = new LandingPage();