var EC = protractor.ExpectedConditions;

var PricingPage = function(){

	this.clickContactHeaderButton = function(){
	element(by.css(".navbar-right [href='/contact']")).click();
	var e = element(by.cssContainingText("h1","Contact"));
	browser.wait(EC.presenceOf(e),10000);
	expect(e.isPresent()).toBeTruthy();
	return require('./contactUsPage.js');
	};

};

module.exports = new PricingPage();