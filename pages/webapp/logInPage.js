/**
 * Created by Ross Bendal on 05/05/2016.
 */

var webUtil = require('../../util/WebUtil.js');

var LogInPage = function(){

    this.fillUpUsername = function(username){
        element(by.css("[name='username']")).sendKeys(username);
        return this;
    };

    this.fillUpPassword = function(password){
        element(by.css("[name='password']")).sendKeys(password);
        return this;
    };

    this.clickSignInPro = function(){
        element(by.css("[type='submit']")).click();
        return require('./pro/homePage.js');
    };

    //********************************************
    // COMPLETE LOG IN PROCESS FUNCTIONS
    //********************************************

    this.signInAsPro = function(){
        element(by.css("[name='username']")).sendKeys("proautomate@yopmail.com");
        element(by.css("[name='password']")).sendKeys("gymcloud");
        element(by.css("[type='submit']")).click();
        webUtil.waitCssUntilVisible(".gc-sidebar-logo[href='#welcome']");
        return require('./pro/homePage.js');
    };

};

module.exports = new LogInPage();