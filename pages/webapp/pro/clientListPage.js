/**
 * Created by rab on 16/05/2016.
 */

var webUtil = require('../../../util/WebUtil.js');

var ClientListPage = function(){

    this.clickAddClientModal = function(){
        element(by.css(".show-client-form .client-title")).click();
        webUtil.waitCssUntilVisible(".modal-header");
        return this;
    };
    this.fillUpClientName = function(text){
        browser.sleep(1000);
        webUtil.waitCssUntilVisible("[placeholder='e.g. John Smith']");
        element(by.css("[placeholder='e.g. John Smith']")).sendKeys(text);
        return this;
    };
    this.clickSave = function(){
        element(by.css("[type='submit']")).click();
        return this;
    };
    
};
module.exports = new ClientListPage();