/**
 * Created by Ross Bendal on 07/05/2016.
 */

var webUtil = require('../../../util/WebUtil.js');
var dataFactory = require('../../../util/dataFactory.js');

var ExerciseListPage = function() {

    this.clickAddExercise = function(){
        element(by.css(".gc-show-add-exercise-modal")).click();
        webUtil.waitCssUntilVisible(".modal-title");
        return this;
    };

    this.fillUpExerciseName = function(text){
        webUtil.waitCssUntilVisible("[maxlength='128']");
        element(by.css("[maxlength='128']")).sendKeys(text);
        return this;
    };
    
    this.clickAddExerciseModal = function(){
        element(by.css("[type='submit']")).click();
        return this;
    };

};

module.exports = new ExerciseListPage();