/**
 * Created by Ross Bendal on 07/05/2016.
 */

var webUtil = require('../../../util/WebUtil.js');

var HomePage = function() {

    this.clickClientSidebar = function(){
        element(by.css(".clients-title")).click();
        webUtil.waitCssUntilVisible(".col-xs-7 .show-client-form");
        return require('./clientListPage.js');
    };

    this.clickExerciseSidebar = function(){
        element(by.css("[data-name='exercises'] .gc-sidebar-cat-name")).click();
        webUtil.waitCssUntilVisible(".gc-show-add-exercise-modal");
        return require('./exerciseListPage.js');
    };

    this.clickWorkoutSidebar = function(){
        element(by.css("[data-name='workout_templates']")).click();
        webUtil.waitCssUntilVisible(".gc-show-add-exercise-modal");
        return require('./workoutListPage.js')
    };

    this.clickProgramSidebar = function(){
        element(by.css("[data-name='program_templates']")).click();
        webUtil.waitCssUntilVisible(".gc-show-add-exercise-modal");
        return require('./programListPage.js')
    };

    this.clickProfileMenu = function(){
        webUtil.waitCssUntilVisible("img[alt='']");
        element(by.css("img[alt='']")).click();
        return this;
    };

    this.clickMenuProfile = function(){
        webUtil.waitCssUntilVisible("[href='#users/me/edit'] .col-xs-9");
        element(by.css("[href='#users/me/edit'] .col-xs-9")).click();
        webUtil.waitCssUntilVisible(".gc-custom[type='submit']");
        return require('./profilePage.js');
    };
    
    this.clickMyTrainingLogSidebar = function(){
        element(by.css("[href='#users/me/'] .gc-sidebar-cat-name")).click();
        return this;
    };

    this.clickHomePageHeaderLogo = function(){
        element(by.css(".gc-sidebar-logo[href='#welcome']")).click();
        return this;
    };
    
};

module.exports = new HomePage();