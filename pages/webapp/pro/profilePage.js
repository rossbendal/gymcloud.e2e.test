/**
 * Created by rab on 03/06/2016.
 */

var webUtil = require('../../../util/WebUtil.js');
var dataFactory = require('../../../util/dataFactory.js');

var ProfilePage = function(){

    this.clickSave = function(){
        webUtil.waitCssUntilVisible("[type='submit']");
        element(by.css("[type='submit']")).click();
        browser.sleep(3000);
    };

    this.fillInWeightInput = function(number){
        webUtil.waitCssUntilVisible("[name='weight']");
        element(by.css("[name='weight']")).clear();
        element(by.css("[name='weight']")).sendKeys(number);
        return this;
    };

    this.fillInBodyFatInput = function(number){
        webUtil.waitCssUntilVisible("[name='bodyfat']");
        element(by.css("[name='bodyfat']")).clear();
        element(by.css("[name='bodyfat']")).sendKeys(number);
        return this;
    };

    this.fillInFirstNameInput = function(name){
        webUtil.waitCssUntilVisible("[name='first_name']");
        element(by.css("[name='first_name']")).clear();
        element(by.css("[name='first_name']")).sendKeys(name);
        return this;
    };

    this.fillInLastNameInput = function(name){
        webUtil.waitCssUntilVisible("[name='last_name']");
        element(by.css("[name='last_name']")).clear();
        element(by.css("[name='last_name']")).sendKeys(name);
        return this;
    };

    this.fillInLocationInput = function(name){
        webUtil.waitCssUntilVisible("[name='location']");
        element(by.css("[name='location']")).clear();
        element(by.css("[name='location']")).sendKeys(name);
        return this;
    };

    this.fillInZIPInput = function(zip){
        webUtil.waitCssUntilVisible("[name='zip']");
        element(by.css("[name='zip']")).clear();
        element(by.css("[name='zip']")).sendKeys(zip);
        return this;
    };

    this.fillInOccupationInput = function(string){
        webUtil.waitCssUntilVisible("[name='employer']");
        element(by.css("[name='employer']")).clear();
        element(by.css("[name='employer']")).sendKeys(string);
        return this;
    };

};
module.exports = new ProfilePage();