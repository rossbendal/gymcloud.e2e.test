/**
 * Created by rab on 15/05/2016.
 */

var webUtil = require('../../../util/WebUtil.js');

var ProgramListPage = function(){

    this.clickAddProgramTemplate = function(){
        element(by.css(".gc-show-add-exercise-modal")).click();
        webUtil.waitCssUntilVisible(".modal-title");
        return this;
    };

    this.fillUpProgramName = function(text){
        webUtil.waitCssUntilVisible("[maxlength='128']");
        element(by.css("[maxlength='128']")).sendKeys(text);
        return this;
    };

    this.clickAddProgramModal = function(){
        element(by.css("[type='submit']")).click();
        return this;
    };

};
module.exports = new ProgramListPage();