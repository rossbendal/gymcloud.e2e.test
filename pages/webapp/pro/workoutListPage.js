/**
 * Created by rab on 14/05/2016.
 */

var webUtil = require('../../../util/WebUtil.js');

var WorkoutListPage = function() {
    
    this.clickAddWorkoutTemplate = function(){
        element(by.css(".gc-show-add-exercise-modal")).click();
        webUtil.waitCssUntilVisible(".modal-title");
        return this;
    };
    
    this.fillUpWorkoutName = function(text){
        webUtil.waitCssUntilVisible("[maxlength='128']");
        element(by.css("[maxlength='128']")).sendKeys(text);
        return this;
    };

    this.clickAddWorkoutModal = function(){
        element(by.css("[type='submit']")).click();
        return this;
    };

    
};
module.exports = new WorkoutListPage();