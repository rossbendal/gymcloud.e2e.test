
var random = require('../../util/dataFactory.js');

var SignUpPage = function(){

    this.fillUpFirstName = function(firstname){
        element(by.css("[name='first_name']")).sendKeys(firstname);
        return this;
    };

    this.fillUpLastName = function(lastname){
        element(by.css("[name='last_name']")).sendKeys(lastname);
        return this;
    };

    this.fillUpEmailAddress = function(email){
        element(by.css("[name='email']")).sendKeys(email);
        return this;
    };

    this.fillUpPassword = function(password){
        element(by.css("[name='password']")).sendKeys(password);
        return this;
    };

    this.fillUpConfirmPassword = function(password){
        element(by.css("[name='password_confirmation']")).sendKeys(password);
        return this;
    };

    this.clickEULACheckbox = function(){
        element(by.css("[type='checkbox']")).click();
        return this;
    };

    this.clickSignUpButton = function(){
        element(by.css("[type='submit']")).click();
        return this;
    };

    //********************************************
    // COMPLETE LOG IN PROCESS FUNCTIONS
    //********************************************

    this.fillUpSignUpForm = function(){
        this.fillUpFirstName(random.newRandomFirstName());
        this.fillUpLastName(random.newRandomLastName());
        this.fillUpEmailAddress(random.newEmailAddress());
        this.fillUpPassword("gymcloud");
        this.fillUpConfirmPassword("gymcloud");
        this.clickEULACheckbox();
        return this;
    };

};

module.exports = new SignUpPage();