describe('As a Guest, I should be able to navigate using header nav - ', function() {
	
	var basePage = require('../../pages/basePage.js');

	beforeEach(function() {
		browser.ignoreSynchronization = true;
	});

	it('from landing to About us', function() {
		var landingPage = basePage.navigateToLandingPage();
		landingPage.clickAboutUsHeaderButton();
	});

	xit('from about us to Pricing', function() {
		var aboutUsPage = basePage.navigateToAboutUsPage();
		aboutUsPage.clickPricingHeaderButton();
	});

	it('from pricing to contact us', function() {
		var pricingPage = basePage.navigateToPricingPage();
		pricingPage.clickContactHeaderButton();
	});

	it('from contact us page to blog', function() {
		var contactUsPage = basePage.navigateToContactUsPage();
		contactUsPage.clickBlogHeaderButton();
	});

});