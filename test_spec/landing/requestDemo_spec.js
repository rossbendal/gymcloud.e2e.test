
describe('As a Guest, I want to inquire to be a tester - ', function() {
	

	var basePage = require('../../pages/basePage.js');
	var landingPage;

	beforeEach(function() {
		browser.ignoreSynchronization = true;
		landingPage = basePage.navigateToLandingPage();
	});

	it('valid request upon information completion.', function() {
		landingPage.fillUpDemoForm();
	});

	it('popup should appear on Try free for 90 days button', function() {
		landingPage.clickTryfreefordaysButton();
	});

});

