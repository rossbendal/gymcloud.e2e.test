/**
 * Created by Ross Bendal on 05/05/2016.
 */

describe('As a Guest, I should be able to contact support form contact us page - ', function () {

    var basePage = require('../../pages/basePage.js');

    it('by filling up the form',function () {
        browser.ignoreSynchronization = true;
        var contactUsPage = basePage.navigateToContactUsPage();
        contactUsPage.fillUpContactForm();
        contactUsPage.clickSubmit();
    });
    
});