/**
 * Created by rab on 01/06/2016.
 */
describe('As a Guest, I should be able to view blog entries - ', function(){

    var basePage = require('../../pages/basePage.js');
    var webUtil = require('../../util/WebUtil.js');
    var blogEntryPage;

    beforeAll(function() {
        browser.ignoreSynchronization = true;
        var blogPage = basePage.navigateToBlogPage();
        blogEntryPage = blogPage.clickReadMoreLink();
    });

    it('and I can see the title.', function() {
        webUtil.waitCssUntilVisible(".single-post__title");
    });

    it('and I can see the who the author is.', function() {
        webUtil.waitCssUntilVisible(".single-details__author");
    });

    it('and I can share it to Facebook', function() {
        blogEntryPage.clickFBShare();
    });

    it('and I can share it to Twitter', function() {
        blogEntryPage.clickTwitterShare();
    });

});