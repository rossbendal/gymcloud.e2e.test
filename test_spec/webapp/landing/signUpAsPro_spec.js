/**
 * Created by Ross Bendal on 07/05/2016.
 */

describe('As a PRO, I should be able to sign up.', function(){

    var basePage = require('../../../pages/basePage.js');
    var webUtil = require('../../../util/WebUtil.js');
    var signUpPage;

    beforeEach(function(){
        browser.ignoreSynchronization = true;
        signUpPage = basePage.navigateToSignUpPage();
    });

    afterAll(function() {
        browser.executeScript('window.sessionStorage.clear();'); //clear session
        browser.executeScript('window.localStorage.clear();'); //clear local storage
        browser.refresh();
    });
    
    it('should NOT accept existing registered email', function(){
        signUpPage.fillUpFirstName("test");
        signUpPage.fillUpLastName("test");
        signUpPage.fillUpEmailAddress("proautomate@yopmail.com");
        signUpPage.fillUpPassword("gymcloud");
        signUpPage.fillUpConfirmPassword("gymcloud");
        signUpPage.clickEULACheckbox();
        signUpPage.clickSignUpButton();

        webUtil.waitTextUntilVisible(".messenger-hidden p","email has already been taken");
    });

    it('should NOT accept mismatching password', function(){
        signUpPage.fillUpFirstName("test");
        signUpPage.fillUpLastName("test");
        signUpPage.fillUpEmailAddress("proauto22341ate@yopmail.com");
        signUpPage.fillUpPassword("gymcloud");
        signUpPage.fillUpConfirmPassword("gymcdloud");
        signUpPage.clickEULACheckbox();
        signUpPage.clickSignUpButton();
        
        webUtil.waitTextUntilVisible(".col-xs-6 label.gc-error-message","The passwords do not match");
    });

    it('should NOT accept invalid password combination',function(){
        signUpPage.fillUpFirstName("test");
        signUpPage.fillUpLastName("test");
        signUpPage.fillUpEmailAddress("proauto22341ate@yopmail.com");
        signUpPage.fillUpPassword("ross");
        signUpPage.fillUpConfirmPassword("ross");
        signUpPage.clickEULACheckbox();
        signUpPage.clickSignUpButton();

        webUtil.waitTextUntilVisible(".gc-invalid label.gc-error-message","Password must be 6 characters");
    });

    it('should NOT accept if EULA is unchecked/not accepted', function(){
        signUpPage.fillUpFirstName("test");
        signUpPage.fillUpLastName("test");
        signUpPage.fillUpEmailAddress("proauto22341ate@yopmail.com");
        signUpPage.fillUpPassword("gymcloud");
        signUpPage.fillUpConfirmPassword("gymcloud");
        signUpPage.clickSignUpButton();

        webUtil.waitCssUntilVisible(".gc-invalid .checkbox label span");
    });

    //*************************
    // Happy Path
    //*************************

    it('should be able to sign-up using valid credentials', function(){
        signUpPage.fillUpSignUpForm();
        signUpPage.clickSignUpButton();

        webUtil.waitCssUntilVisible(".presets-modal-header");
    });

});