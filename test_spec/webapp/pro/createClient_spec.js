/**
 * Created by rab on 16/05/2016.
 */

describe('As a PRO, I should be able to add new clients',function(){

    var dataFactory = require('../../../util/dataFactory.js');
    var webUtil = require('../../../util/WebUtil.js');
    var basePage = require("../../../pages/basePage.js");
    var homePage,
        clientListPage;

    beforeAll(function(){
        browser.ignoreSynchronization = true;
        var logInPage = basePage.navigateToLogInPage();
        homePage = logInPage.signInAsPro();
    });

    afterAll(function(){
        browser.executeScript('window.sessionStorage.clear();'); //clear session
        browser.executeScript('window.localStorage.clear();'); //clear local storage
        browser.refresh();
    });

    beforeEach(function(){
        clientListPage = homePage.clickClientSidebar();
        clientListPage.clickAddClientModal();
    });

    it('using valid name and with NO invite process', function(){
        clientListPage.fillUpClientName(dataFactory.setRandomClientName());
        clientListPage.clickSave();


        webUtil.waitTextUntilVisible(".messenger-hidden p","Client successfully saved");
        webUtil.verifyTextFromList(".gc-clients-list-ind .gc-clients-list-client-link",dataFactory.getRandomClientName());
    });

    it('but NOT with single name only',function(){
        clientListPage.fillUpClientName("wasapp");
        clientListPage.clickSave();
        webUtil.waitTextUntilVisible(".gc-error-message","Please enter a first name and a last name");
    });

});