/**
 * Created by rab on 15/05/2016.
 */

describe('As a PRO, I should be able to create a new Program',function(){

    var dataFactory = require('../../../util/dataFactory.js');
    var webUtil = require('../../../util/WebUtil.js');
    var basePage = require("../../../pages/basePage.js");

    beforeEach(function(){
        browser.ignoreSynchronization = true;
    });

    afterAll(function() {
        browser.executeScript('window.sessionStorage.clear();'); //clear session
        browser.executeScript('window.localStorage.clear();'); //clear local storage
        browser.refresh();
    });
    
    it('by clicking "Add New Program", adding a name, and seeing it in list.', function(){
        var logInPage = basePage.navigateToLogInPage();
        var homePage = logInPage.signInAsPro();
        var programListPage = homePage.clickProgramSidebar();
        programListPage.clickAddProgramTemplate();
        programListPage.fillUpProgramName(dataFactory.setRandomProgramName());
        programListPage.clickAddProgramModal();

        webUtil.waitTextUntilVisible(".messenger-hidden p","New item added");
        webUtil.verifyTextFromList(".item a.gc-exercises-link",dataFactory.getRandomProgramName());
    });

});