/**
 * Created by Ross Bendal on 10/14/2016.
 */

describe('As a PRO, I should be able to create a new workout',function(){

    var dataFactory = require('../../../util/dataFactory.js');
    var webUtil = require('../../../util/WebUtil.js');
    var basePage = require("../../../pages/basePage.js");
    
    beforeEach(function(){
        browser.ignoreSynchronization = true;
    });

    afterAll(function() {
        browser.executeScript('window.sessionStorage.clear();'); //clear session
        browser.executeScript('window.localStorage.clear();'); //clear local storage
        browser.refresh();
    });
    
    it('by clicking "Add New", adding workout name and seeing it in list',function() {
        var logInPage = basePage.navigateToLogInPage();
        var homePage = logInPage.signInAsPro();
        var workoutListPage = homePage.clickWorkoutSidebar();
        workoutListPage.clickAddWorkoutTemplate();
        workoutListPage.fillUpWorkoutName(dataFactory.setRandomWorkoutName());
        workoutListPage.clickAddWorkoutModal();

        webUtil.waitTextUntilVisible(".messenger-hidden p","New item added");
        webUtil.verifyTextFromList(".item a.gc-exercises-link",dataFactory.getRandomWorkoutName());
    });
});

