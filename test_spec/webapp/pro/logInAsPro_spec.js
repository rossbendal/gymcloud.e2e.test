/*
 * Created by Ross Bendal on 07/05/2016.
 */

describe('As a PRO, I should be able to log in', function() {

    var webUtil = require('../../../util/WebUtil.js');
    var basePage = require("../../../pages/basePage.js");
    var logInPage;

    beforeEach(function() {
        browser.ignoreSynchronization = true;
        logInPage = basePage.navigateToLogInPage();
    });
    
    afterAll(function() {
        browser.executeScript('window.sessionStorage.clear();'); //clear session
        browser.executeScript('window.localStorage.clear();'); //clear local storage
        browser.refresh();
    });
    
    it('but NOT with invalid credentials.', function(){
        logInPage.fillUpUsername("invalid");
        logInPage.fillUpPassword("invaild");
        logInPage.clickSignInPro();

        webUtil.waitTextUntilVisible(".messenger-hidden p","Invalid Credentials");
    });

    it('using valid credentials', function(){
        logInPage.fillUpUsername("proautomate@yopmail.com");
        logInPage.fillUpPassword("gymcloud");
        logInPage.clickSignInPro();

        webUtil.waitCssUntilVisible("#gc-topnav-search");
        
    });
});
