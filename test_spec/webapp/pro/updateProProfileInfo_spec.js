/**
 * Created by rab on 03/06/2016.
 */

describe('As a PRO, I should be able to update my Profile', function() {

    var webUtil = require('../../../util/WebUtil.js');
    var basePage = require("../../../pages/basePage.js");
    var dataFactory = require('../../../util/dataFactory.js');
    var homePage;
    var profilePage;

    beforeAll(function(){
        browser.ignoreSynchronization = true;
        var logInPage = basePage.navigateToLogInPage();
        homePage = logInPage.signInAsPro();
        homePage.clickProfileMenu();
        profilePage = homePage.clickMenuProfile();
    });

    afterAll(function() {
        browser.executeScript('window.sessionStorage.clear();'); //clear session
        browser.executeScript('window.localStorage.clear();'); //clear local storage
        browser.refresh();
    });
    
    it('by editing Weight field.',function(){
        profilePage.fillInWeightInput(dataFactory.setRandomWholeNumber());
        profilePage.clickSave();
        expect(element(by.css("[name='weight']")).getAttribute('value')).toEqual(dataFactory.getRandomWholeNumber()+".0");
    });

    it('by editing BodyFat field.',function(){
        profilePage.fillInBodyFatInput(dataFactory.setRandomWholeNumber());
        profilePage.clickSave();
        expect(element(by.css("[name='bodyfat']")).getAttribute('value')).toEqual(dataFactory.getRandomWholeNumber()+".0");
    });

    it('by editing First Name field.',function(){
        profilePage.fillInFirstNameInput(dataFactory.newRandomFirstName());
        profilePage.clickSave();
        expect(element(by.css("[name='first_name']")).getAttribute('value')).toEqual(dataFactory.getRandomFirstName());
    });

    it('by editing Last Name field.',function(){
        profilePage.fillInLastNameInput(dataFactory.newRandomLastName());
        profilePage.clickSave();
        expect(element(by.css("[name='last_name']")).getAttribute('value')).toEqual(dataFactory.getRandomLastName());
    });

    it('by editing Location field.',function(){
        profilePage.fillInLocationInput(dataFactory.setHomeAddress());
        profilePage.clickSave();
        expect(element(by.css("[name='location']")).getAttribute('value')).toEqual(dataFactory.getHomeAddress());
    });

    it('by editing ZIP field.',function(){
        profilePage.fillInZIPInput(dataFactory.setZip());
        profilePage.clickSave();
        expect(element(by.css("[name='zip']")).getAttribute('value')).toEqual(dataFactory.getZip());
    });

    it('by editing occupation field.',function(){
        profilePage.fillInOccupationInput(dataFactory.setOccupation());
        profilePage.clickSave();
        expect(element(by.css("[name='employer']")).getAttribute('value')).toEqual(dataFactory.getOccupation());
    });



});