/**
 * Created by rab on 02/05/2016.
 */

var EC = protractor.ExpectedConditions;


var WebUtil = function () {

    this.waitCssUntilVisible = function (elements) {
        var e = element(by.css(elements));
        browser.wait(EC.presenceOf(e),60000);
        expect(e.isPresent()).toBe(true);
    };

    this.waitTextUntilVisible = function (elements, text) {
        var e = element(by.cssContainingText(elements,text));
        browser.wait(EC.presenceOf(e),40000);
        expect(e.isPresent()).toBe(true);
    };

    this.verifyTextFromList = function (elements,item){
        expect(element.all(by.css(elements)).getText()).toContain(item);
    };

};

module.exports = new WebUtil();