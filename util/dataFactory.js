/**
 * Created by Ross Bendal on 07/05/2016.
 */

var random = require('random-name');
var randomWord = require('random-word');
var firstName;
var lastName;
var emailAddress;
var homeAddress;
var zip;
var occupation;
var exerciseName;
var workoutName;
var programName;
var clientName;
var randomWholeNumber;

var DataFactory = function(){

    this.newRandomFirstName = function() {
        firstName = random.first();
        return firstName;
    };

    this.getRandomFirstName = function() {
        return firstName;
    };

    this.newRandomLastName = function() {
        lastName = random.last();
        return lastName;
    };

    this.getRandomLastName = function() {
        return lastName;
    };

    this.newEmailAddress = function() {
        emailAddress = ((firstName+lastName)+"@yopmail.com").toLocaleLowerCase();
        return emailAddress;
    };

    this.setRandomExerciseName = function() {
        exerciseName = (randomWord()+"-exercise").toUpperCase();
        return exerciseName;
    };
    
    this.getRandomExerciseName = function() {
        return exerciseName;
    };

    this.setRandomWorkoutName = function() {
        workoutName = (randomWord()+"-workout").toUpperCase();
        return workoutName;
    };

    this.getRandomWorkoutName = function() {
        return workoutName;
    };
    this.setRandomProgramName = function() {
        programName = (randomWord()+"-program").toUpperCase();
        return programName;
    };
    this.getRandomProgramName = function() {
        return programName;
    };

    this.setRandomClientName = function() {
        clientName = (""+random.first()+" "+random.last()+"").toUpperCase();
        return clientName;
    };
    this.getRandomClientName = function() {
        return clientName;
    };

    this.setRandomWholeNumber = function() {
        randomWholeNumber = Math.floor((Math.random() * 100) + 1);
        return randomWholeNumber;
    };

    this.getRandomWholeNumber = function() {
        return randomWholeNumber;
    };

    this.setHomeAddress = function() {
        var x = Math.floor((Math.random() * 100) + 1);
        homeAddress = (x.toString()+" Palm Springs");
        return homeAddress;
    };

    this.getHomeAddress = function() {
        return homeAddress;
    };

    this.setZip = function() {
        zip = Math.floor((Math.random() * 100) + 1000).toString();
        return zip;
    };

    this.getZip = function() {
        return zip;
    };

    this.setOccupation = function() {
        occupation = (randomWord()+" engineer").toUpperCase();
        return occupation;
    };

    this.getOccupation = function() {
        return occupation;
    };
};

module.exports = new DataFactory();